package com.luxoft.dnepr.courses.unit1;

import junit.framework.Assert;
import org.junit.Test;

/**
 * Created with IntelliJ IDEA.
 * User: user
 * Date: 03.10.13
 * Time: 17:41
 * To change this template use File | Settings | File Templates.
 */
public class LuxoftUtilsTest {

    @Test
    public void testHello() {
        Assert.assertEquals("Hi Sasha", LuxoftUtils.hello("Sasha"));
        Assert.assertEquals("Hi Vova", LuxoftUtils.hello("Vova"));
        Assert.assertEquals("Hi Pethya", LuxoftUtils.hello("Pethya"));
    }

    @Test
    public void testHelloError() {
        Assert.assertEquals("I don't know you", LuxoftUtils.hello(null));
        Assert.assertEquals("Hi", LuxoftUtils.hello(""));
    }
}
