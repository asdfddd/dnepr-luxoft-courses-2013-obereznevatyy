package com.luxoft.dnepr.courses.unit1;

/**
 * Created with IntelliJ IDEA.
 * User: user
 * Date: 03.10.13
 * Time: 17:36
 * To change this template use File | Settings | File Templates.
 */
public final class LuxoftUtils {
    private LuxoftUtils() {
    }

    public static String hello(String name) {
        if (name == null) {
                 return "I don't know you";
        }
        return ("Hi " + name).trim();
    }
}
